package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class TranslationController {

    private static final String template = "Tłumaczenie: %s";
    private final AtomicLong counter = new AtomicLong();
    Map<String,String> dictionary = new HashMap<>();

    @RequestMapping("/translation")
            public Translation translate(@RequestParam(value="word", defaultValue="Nie_podane_słowo") String word) {
                readFile();
                String wordTranslation =  dictionary.get(word);
                if (wordTranslation == null){
                    wordTranslation = "Słowo_nie_występuje_w_słowniku";
                }
                return new Translation(counter.incrementAndGet(),
                            String.format(template, wordTranslation));
    }


    private void readFile(){
        Path homeDir = Paths.get(".","src","main","java","hello");//("C:","work");
        Path pathToFile = homeDir.resolve("dictionary.txt");
        //pathToFile.toFile();
        //File pathToFile2 = pathToFile.toFile();

        String current = null;
        try {
            current = new java.io.File( "." ).getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Current dir:"+current);

        //dla bardzo duzych plikow korzystamy z BaufferedReadera
        List<String> fileLines = null;
        try {
            fileLines = Files.readAllLines(pathToFile, Charset.forName("CP1250"));
            for (String fileLine : fileLines) {

                String[] wordList = fileLine.split(" ");
                dictionary.put(wordList[0],wordList[1]);
            }
        } catch (IOException e) {
            System.out.println("File reading failed");
            e.printStackTrace();
        }
    }

}
